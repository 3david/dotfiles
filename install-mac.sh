#!/bin/bash

./modules/git/install.sh
./modules/zsh/install.sh
./modules/ack-grep/install.sh
./modules/ruby/post-install.sh
./modules/nodejs.sh
./modules/mac-smb-performance-fix/install.sh
