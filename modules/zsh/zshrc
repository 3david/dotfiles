# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
#ZSH_THEME="terminalparty"
#ZSH_THEME="gnzh"
ZSH_THEME="af-magic"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
#plugins=(git)
plugins=(git rails3 mercurial python bundler)
# IMPORTANT: the 'gem' and 'rvm' plugins cause a slowdown

source $ZSH/oh-my-zsh.sh

# required for bundler to work correctly
# without these it throws "ArgumentError: invalid byte sequence in US-ASCII"
# whenever it finds a gemspec with non-unicode characters
# (see http://ruckus.tumblr.com/post/18613786601/bundler-install-error-argumenterror-invalid-byte)
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin
PATH=$PATH:$HOME/.rvm/bin:$HOME/.config/dotfiles/scripts
PATH=$PATH:$HOME/bin:$HOME/projects/railsnew:$HOME/scripts
PATH=$PATH:/usr/local/share/python

export MPD_HOST=bob@localhost

# ALIASES
alias m=ncmpcpp
alias x=exit
alias mf='mpc search filename'
alias ma='mpc add'
alias mp='mpc play'
alias mcl='mpc clear'
alias sr='screen -R'
alias ak='ssh-add ~/.keys/bitbucket.ssh.private'
alias cl='xclip -selection clipboard -o'
alias ft="ruby ~/projects/tvutils/bin/filestube.rb -s filesonic"
alias sub="ruby ~/projects/tvutils/bin/download_subtitles.rb"
alias clean="ruby ~/projects/tvutils/bin/clean.rb"
alias st='scrot -s'
export DOTF=~/.config/dotfiles
alias dotf='cd ~/.config/dotfiles'
export DOTV=~/.config/dotvim
alias dotv='cd ~/.config/dotvim'
export DOTS=~/Library/Application\ Support/Sublime\ Text\ 2
alias dots='cd $DOTS'
alias rr='source ~/.config/dotfiles/scripts/init-rvm'
alias mac='ruby ~/projects/macrumors/macrumors.rb'
alias wp='feh --bg-fill'
alias pryy='pry -r ./config/environment'
alias trailer='wget -U QuickTime'
alias txt='cd ~/Dropbox/PlainText && vim'

if [[ "`uname -s`" == "Darwin" ]]; then
  alias ls='ls -FGsk'
  # s = show size
  # k = show size in kilobytes
  alias ackc='ack --coffee'
else
  alias ack='ack-grep'
  alias ackc='ack-grep --coffee'
  alias ls='ls --color=always -XFhs'
fi

alias wip='cucumber --drb --profile wip'
alias cuc='cucumber --drb'
alias tigs='tig status'
alias ts='tig status'
alias z='zeus'
alias t='tmux -u'

# so tmux will allow 256 colors:
if [[ "$TERM" != "screen-256color" ]]; then
  export TERM=xterm-256color
fi

[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
rvm default

export SHORT_HOSTNAME=`short-hostname`

unsetopt correctall
